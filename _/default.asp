<!--#include virtual="/common/include/inc.header.asp"-->
<h1>Ahern &amp; Kill</h1>
<h2>Committed to Corporate Assets: Protection - Retention - Collection.</h2>
<p>
The last several years have created unparalleled economic challenges to all businesses in southeastern Michigan; at this juncture it appears that those challenges will not abate for some period of time.
Ahern &amp; Kill's 50 plus years of business and legal experience gives our clients the tools they need to respond to this rapidly changing business environment.
</p>
<br/>
<table border="0" cellpadding="0" cellspacing="0" style="width:531px;">
<tr>
	<td valign="top" align="center" style="width:131px;">
		<!-- begin super lawyers badge -->
		<div style="background-image:url('/images/bg-superlawyer.png');width:111px;height:54px;padding-top:44px;border:solid 1px black;">
		<center>
		<p style="margin-top:0px;margin-bottom:4px;"><a href="http://www.superlawyers.com/redir?r=http://www.superlawyers.com/michigan/lawyer/Joseph-A-Ahern/d56f3f52-a678-4432-9d26-b55b2d3427da.html&amp;c=120_white_badge&amp;i=d56f3f52-a678-4432-9d26-b55b2d3427da" title="View the profile of Michigan Business Litigation Attorney Joseph A. Ahern" style="text-decoration:none; margin:0; padding:0; line-height:1; text-align:center; font-family:arial,sans-serif; color:rgb(255,145,0); font-size:10px; font-weight:bold; outline:none; border:none; text-shadow:none;">Joseph A. Ahern</a></p>
		<p style="margin-top:10px;margin-bottom:3px;font-size:14px;"><a style="text-decoration:none;" href="http://www.superlawyers.com/search?q=Ahern+Fleury&l=michigan">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></p>
		</center>
		</div>
		<!-- end super lawyers badge -->
	</td>
	<td valign="top" align="center" style="width:200px;"><a target="_blank" href="http://www.dbusiness.com/DBusiness/Top-Lawyers-2012/index.php/search/Fleury/searchmode/partial/"><img src="/images/home-dbusiness.gif" border="0" width="180" height="106"/></a></td>
	<td valign="top" align="center" style="width:200px;"><a target="_blank" href="http://www.martindale.com/Ahern-Fleury/law-firm-28393311.htm"><img src="/images/home-martindale.gif" border="0" width="180" height="106"/></a></td>
</tr>
</table>
<!--#include virtual="/common/include/inc.footer.asp"-->