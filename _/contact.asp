<!--#include virtual="/common/include/inc.header.asp"-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function map_initialize() {
        var latlng = new google.maps.LatLng(42.549636, -83.216764);
        var myOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("contact_map"), myOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Ahern &amp; Kill\n355 S. Old Woodward\nSuite 210\nBirmingham, MI 48009"
        });          
    }

</script>

<h1>Contact Us</h1>
Ahern &amp; Kill<br />
355 S. Old Woodward<br />
Suite 210<br />
Birmingham, MI 48009<br />
<br />
<table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>Phone:&nbsp;</td>
        <td>(248) 723-6101</td>
    </tr>
    <tr>
        <td>Fax:&nbsp;</td>
        <td>(248) 723-6102</td>
    </tr>
    <!--
    <tr>
        <td>Email:&nbsp;</td>
        <td> (for general inquiries)</td>
    </tr>
    -->
</table>
<p>
For general inquries, please email <a href="mailto:jahern@ahernkill.com">jahern@ahernkill.com</a>.
</p>
<p>
<a target="_blank" href="http://maps.google.com/maps?hl=en&safe=off&um=1&ie=UTF-8&q=Ahern+Kill,+Birmingham+MI&fb=1&split=1&gl=us&cid=0,0,13345557030423469346&ei=BUFKSqONB5DiNdzJxLMK&sa=X&oi=local_result&ct=image&resnum=1">Click here for driving directions</a></p>
<div id="contact_map"></div>
<!--
<script type="text/javascript">
    map_initialize();
</script>
-->
<script type="text/javascript">
$(document).ready(function(){
    var latlng = new google.maps.LatLng(42.549636, -83.216764);
    var myOptions = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("contact_map"), myOptions);
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "Ahern &amp; Kill\n355 S. Old Woodward\nSuite 210\nBirmingham, MI 48009"
    });
});
</script>
<!--#include virtual="/common/include/inc.footer.asp"-->