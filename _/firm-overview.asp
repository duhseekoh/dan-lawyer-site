<!--#include virtual="/common/include/inc.header.asp"-->
<h1>Firm Overview</h1>
<p>
Ahern &amp; Kill's three attorneys bring a unique perspective to the practice of business law.  Each of the firm's three lawyers has an extensive business background, including service on many businesses and non business boards of directors, active business experience and over 50 years of counseling business clients.  Ahern &amp; Kill is cognizant that the best solutions to legal problems for its clients are frequently business, and not legal solutions. It is our opinion that most business law firms spend far too much of their clients' resources (both time and money) in addressing their clients' legal needs. Ahern &amp; Kill's experienced attorneys reach creative solutions in a value added approach to their clients' business and legal problems.
</p>
<!--#include virtual="/common/include/inc.footer.asp"-->