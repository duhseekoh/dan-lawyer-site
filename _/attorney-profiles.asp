<!--#include virtual="/common/include/inc.header.asp"-->
<h1>Attorney Profiles</h1>
<%
Dim arrBio
Dim arrBioName
Dim arrBioAbstract
Dim intCtr
Dim strBio, varBio
Dim strBioContact 'Used on inc.footer.asp

arrBio = Array("joseph-ahern", "amanda-kill", "daniel-dicicco") '"carey-sienkiewicz", "jennifer-meyers")
arrBioName = Array("Joseph A. Ahern", "Amanda A. Kill", "Daniel C. DiCicco") '"Carey L. Sienkiewicz", "Jennifer L. Meyers, Office Manager")
arrBioEmail = Array("jahern@ahernkill.com", "akill@ahernkill.com", "ddicicco@ahernkill.com") '"csienkiewicz@ahernkill.com", "jmeyers@ahernkill.com")
arrBioAbstract = Array("JOSEPH A. AHERN received his Bachelor of Arts degree, majoring in both economics and history, from Marquette University in 1980, and his Juris Doctor degree from Marquette University in 1984. After practicing law", _
    "AMANDA A. KILL received her Bachelors of Arts degree, majoring in both political science and public communications with a minor in sociology, from Syracuse University in 2002, and her Juris Doctorate Degree", _
    "DANIEL C. DICICCO received his Bachelors of Arts degree from Purdue University in 2004, and his Juris Doctor cum laude from Wayne State University Law School in 2007")
	'"CAREY L. SIENKIEWICZ received two Bachelor of Arts degrees, in Political Science and Criminal Justice, from Michigan State University in 2002 and her Juris Doctorate Degree from Wayne State University Law School in 2010", _
	'"JENNIFER L. MEYERS is a Michigan licensed attorney who has transitioned into administrative law office management.  Prior to joining Ahern & Kill in 2012 Jennifer Meyers owned and operated her own general practice")
	'"MICHAEL J. BILL concentrates his practice in the areas of contract disputes and commercial transactions with an emphasis on commercial collection matters. Mr. Bill graduated cum laude from the University of Michigan", _
	'"JEFFREY J. FLEURY, a 1988 Brother Rice High School graduate, received his Bachelors of Arts degree from Michigan State University in 1992. In 1995, Mr. Fleury received his Juris Doctor degree cum laude", _
	
strBio = Request.QueryString("bio")
'Validate that this bio is valid
For Each varBio In arrBio
    If strBio = varBio Then
        blnValidBio = True
        Exit For
    End If
Next
%>
<%
If blnValidBio Then
    Server.Execute("/bios/" & strBio & ".html")
Else
    For intCtr = 0 To UBound(arrBio)
	'	If intCtr = UBound(arrBio) Then
	'		Response.Write "<h1>Our Staff</h1>" & vbCrLf
	'	End If
    %>
    <div class="bio">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="top"><a href="/attorney-profiles.asp?bio=<%=arrBio(intCtr)%>" class="bio_link"><img src="/images/bio-<%=arrBio(intCtr)%>.jpg" /></a></td>
                <td valign="top">
                    <div class="bio_name"><%=arrBioName(intCtr)%></div>
                    <div class="bio_abstract">
                    <%=arrBioAbstract(intCtr)%>...
                    </div>
                    <a href="/attorney-profiles.asp?bio=<%=arrBio(intCtr)%>" class="bio_link">Read More</a>
                </td>
            </tr>
        </table>
    </div>
    <%
    Next
End If
%>
<!--#include virtual="/common/include/inc.footer.asp"-->