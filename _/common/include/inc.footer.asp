﻿                        </div>
                    </div>
                    <div id="main_blurb_bottom">&nbsp;</div>
                </div>
				</td>
				<td valign="top">						
                <div id="callout">
                    <img src="/images/callout-<%=strKeyword%>.jpg" /><br />
                    <%If strKeyword <> "contact" Then%>
                    <div id="callout_text_top">&nbsp;</div>
                    <div id="callout_text">
                        <div>
                        <h1>Contact Us</h1>
						<%
						If blnValidBio Then
							'Now determine the name of the biography
							For intCtr = 0 To UBound(arrBio)
								If strBio & "x" = arrBio(intCtr) & "x" Then
									strBioContact = arrBioName(intCtr)
									strContactEmail = arrBioEmail(intCtr) 'Override default contact email
									Response.Write "<strong>" & strBioContact & "</strong><br/>"
									Exit For
								End If
							Next
						Else
							'Default contact email
							strContactEmail = "jahern@ahernkill.com"
						End If
						%>
                        Ahern &amp; Kill<br />
                        355 S. Old Woodward<br />
                        Suite 210<br />
                        Birmingham, MI 48009<br />
                        <br />
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td>Phone:&nbsp;</td>
                                <td>(248) 723-6101</td>
                            </tr>
                            <tr>
                                <td>Fax:&nbsp;</td>
                                <td>(248) 723-6102</td>
                            </tr>
                            <tr>
                                <td colspan="2"><%=strContactEmail%></td>
                                <!--<td></td>-->
                            </tr>
                       </table>
                       <br />
                       <a target="_blank" href="http://maps.google.com/maps?hl=en&safe=off&um=1&ie=UTF-8&q=Ahern+Kill,+Birmingham+MI&fb=1&split=1&gl=us&cid=0,0,13345557030423469346&ei=BUFKSqONB5DiNdzJxLMK&sa=X&oi=local_result&ct=image&resnum=1">Click here for driving directions</a></p>
                        </div>
                    </div>
                    <div id="callout_text_bottom">&nbsp;</div>
                    <%End If%>
                </div>
				</td>
			</tr>
			</table>
            <!-- End Main Content -->
            </div>
            <div id="footer">
                <div id="footer_left"><div style="padding:11px;">COPYRIGHT &copy; 2009 - <%=Year(Now())%></div></div>
                <div id="footer_right"><div id="nav"><a href="/firm-overview.asp"><img src="/images/ftr-firm-overview.gif" width="91" height="34" alt=""/></a><a href="/attorney-profiles.asp"><img src="/images/ftr-attorney-profiles.gif" width="118" height="34" alt=""/></a><a href="/practice-areas.asp"><img src="/images/ftr-practice-areas.gif" width="101" height="34" alt=""/></a><a href="/contact.asp"><img src="/images/ftr-contact.gif" width="86" height="34" alt=""/></a></div></div>
            </div>            
        </div>
    </div>
</center>
</body>
</html>
