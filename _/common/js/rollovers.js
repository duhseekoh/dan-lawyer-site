		
	$(document).ready(function() {
		
		// Preload all rollovers
		$("#nav img").each(function() {
			// Set the original src
			rollsrc = $(this).attr("src");
			rollON = rollsrc.replace(/.gif$/ig,"_on.gif");
			$("<img>").attr("src", rollON);
		});
		
		// Navigation rollovers
		$("#nav a").mouseover(function(){
			imgsrc = $(this).children("img").attr("src");
			matches = imgsrc.match(/_on/);
			
			// don't do the rollover if state is already ON
			if (!matches) {
			imgsrcON = imgsrc.replace(/.gif$/ig,"_on.gif"); // strip off extension
			$(this).children("img").attr("src", imgsrcON);
			}
			
		});
		$("#nav a").mouseout(function(){
			$(this).children("img").attr("src", imgsrc);
		});
		
	
	});