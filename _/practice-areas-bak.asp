<!--#include virtual="/common/include/inc.header.asp"-->
<h1>Practice Areas</h1>
<p>
<b>Bankruptcy/Insolvency</b><br />
We assist our clients navigate the complex and often counter-intuitive world of the bankruptcy code and various insolvency statutes
<br /><br />
<b>Business Law</b><br />
Our lawyers have counseled hundreds of small to medium sized businesses over the past twenty-five years.  When consulting our clients, we understand both the cost of our time and the value of our client's time.
<br /><br />
<b>Collections</b><br />
Ahern Fleury represents many clients in commercial disputes involving debt collections and contractual money disputes. We specialize in the areas of pre-suit collections and work-outs, collections litigation and post-judgment collections. 
<br /><br />
<b>Construction Law</b><br />
Ahern Fleury's attorneys have extensive experience in all aspects of construction law practice. We have represented many different trades and service providers in the construction industry, in both litigation and non litigation contexts. 
<br /><br />
<b>Corporate Restructuring</b><br />
We have assisted hundreds of clients as they have adapted to the new economic and banking climates in Southeastern Michigan and across the United States.  We believe this new environment dictates a newer and linear approach to solving problems.
<br /><br />
<b>Estate Planning</b><br />
We have helped hundreds of our clients protect their life's work for the next generation, covering both business and non-business assets.
<br /><br />
<b>Family Law</b><br />
We protect our client's interests, while preserving the fragile family ties in all family law matters.
<br /><br />
<b>General Civil Litigation</b><br />
We represent our clients in business disputes throughout North America in both federal and state litigations forums.
</p>
<!--#include virtual="/common/include/inc.footer.asp"-->